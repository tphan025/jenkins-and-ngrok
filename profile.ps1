function jenkins-start {
	$jdir = "D:\Jenkins CICD\"
	cd $jdir
	java -jar jenkins.war --httpPort=8080
}
function ngrok {
	$ndir = "D:\Jenkins CICD\"
	cd $ndir
	./ngrok.exe http 8080
}
function jenkins-ngrok {
	param (
	[parameter(Mandatory=$true)]
	[String]
	$username,
	[parameter(Mandatory=$true)]
	[String]
	$slug,
	[String]
	$password=''
	)
	invoke-expression 'cmd /c start powershell -Command { ngrok }'
	invoke-expression 'cmd /c start powershell -NoExit -Command { jenkins-start }'
	echo "Sleeping for 10s to wait for ngrok to start"
	Start-sleep -s 10
	set-webhook $username $slug $password
}
function refresh {
	Invoke-Command { & "powershell.exe" } -NoNewScope
}
function set-webhook {
	param (
	[parameter(Mandatory=$true)]
	[String]
	$username,
	[parameter(Mandatory=$true)]
	[String]
	$slug,
	[String]
	$password=''
	)
	$basicauth = "$($username):$($password)"
	$encoded = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($basicauth))
	echo "Basic $encoded"

	$ngrokresponse = Invoke-RestMethod 'http://localhost:4040/api/tunnels' -Method 'GET' -Headers $headers -Body $body
	$ngrokresponse | ConvertTo-Json > $null
	$tunnel_url = $ngrokresponse.tunnels[1].public_url
	echo "tunnel url: $tunnel_url"

	$headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
	$headers.Add("Content-Type", "application/vnd.api+json")
	$headers.Add("Authorization", "Basic $encoded")
	$url = "https://api.bitbucket.org/2.0/repositories/$($username)/$($slug)/hooks/"
	$hres = Invoke-RestMethod $url -Method 'GET' -Headers $headers
	$hres | ConvertTo-Json > $null
	$hookid = $hres.values[0].uuid

	$url = "https://api.bitbucket.org/2.0/repositories/$($username)/$($slug)/hooks/$($hookid)"
	$body = "    {
		`n      `"url`": `"$tunnel_url/generic-webhook-trigger/invoke?token=hookthis`",
		`n      `"description`": `"Updated automatically with api`",
		`n      `"active`": true,
		`n      `"events`": [
		`n        `"repo:push`"
		`n      ]
		`n
	}"

	$response = Invoke-RestMethod $url -Method 'PUT' -Headers $headers -Body $body
	$response | ConvertTo-Json > $null
	echo "-----Updated bitbucket webhook url with the new ngrok tunnel : $tunnel_url-----"
}
