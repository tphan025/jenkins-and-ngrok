# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository provide a powershell profile script for easy integration between jenkins and bitbucket when you are running your jenkins ci server locally behind a firewall and require a third-party app like ngrok to receive webhooks, The script uses ngrok and bitbucket api calls
* Version 1.0.1

### How do I get set up? ###

* Create a webhook in your repository
* Download and install jenkins
* Download ngrok
* Specify your path to jenkins.war file and ngrok.exe in the profile script
* Copy the code in this repository to your powershell profile script (with `notepad $profile`)
* reload the profile with .$profile
* run `jenkins-ngrok <bitbucket-username> <repo-slug> <bitbucket-password>`

### Note:
* The first webhook in your repo will be updated, this can be changed ofcourse
* This script assumes the default api endpoint for ngrok to be at localhost:4040